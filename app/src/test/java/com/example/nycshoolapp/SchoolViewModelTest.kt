import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycshoolapp.data.model.SatScore
import com.example.nycshoolapp.data.model.School
import com.example.nycshoolapp.data.repository.SchoolRepository
import com.example.nycshoolapp.domain.NetworkStatus
import com.example.nycshoolapp.viewmodel.SchoolViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class SchoolViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val testDispatcher = StandardTestDispatcher()

    @Mock
    private lateinit var repository: SchoolRepository

    private lateinit var viewModel: SchoolViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `loadSchoolData success`() = runTest {
        // Given
        val mockSchools = listOf(School("001", "School 1", "", "", "", "", "", "", "", "", ""))
        val mockSatScores = listOf(SatScore("001", "500", "500", "500", "School 1"))
        `when`(repository.getSchools()).thenReturn(NetworkStatus.Success(mockSchools))
        `when`(repository.getSatScores()).thenReturn(NetworkStatus.Success(mockSatScores))

        // When
        viewModel = SchoolViewModel(repository)
        testDispatcher.scheduler.advanceUntilIdle()

        // Then
        val schoolsResult = viewModel.schools.first()
        val satScoresResult = viewModel.satScores.first()

        assertTrue(schoolsResult is NetworkStatus.Success)
        assertEquals(mockSchools, (schoolsResult as NetworkStatus.Success).data)

        assertTrue(satScoresResult is NetworkStatus.Success)
        assertEquals(mockSatScores.associateBy { it.dbn }, (satScoresResult as NetworkStatus.Success).data)
    }

    @Test
    fun `loadSchoolData error`() = runTest {
        // Given
        val errorMessage = "Network error"
        `when`(repository.getSchools()).thenReturn(NetworkStatus.Error(errorMessage))
        `when`(repository.getSatScores()).thenReturn(NetworkStatus.Error(errorMessage))

        // When
        viewModel = SchoolViewModel(repository)
        testDispatcher.scheduler.advanceUntilIdle()

        // Then
        val schoolsResult = viewModel.schools.first()
        val satScoresResult = viewModel.satScores.first()

        assertTrue(schoolsResult is NetworkStatus.Error)
        assertEquals(errorMessage, (schoolsResult as NetworkStatus.Error).exception)

        assertTrue(satScoresResult is NetworkStatus.Error)
        assertEquals(errorMessage, (satScoresResult as NetworkStatus.Error).exception)
    }

    @Test
    fun `getSatScoreForSchool returns correct score`() = runTest {
        // Given
        val mockSatScores = listOf(
            SatScore("001", "500", "500", "500", "School 1"),
            SatScore("002", "600", "600", "600", "School 2")
        )
        `when`(repository.getSatScores()).thenReturn(NetworkStatus.Success(mockSatScores))

        // When
        viewModel = SchoolViewModel(repository)
        testDispatcher.scheduler.advanceUntilIdle()

        // Then
        val satScore = viewModel.getSatScoreForSchool("001")
        assertNotNull(satScore)
        assertEquals("500", satScore?.sat_writing_avg_score)
        assertEquals("500", satScore?.sat_math_avg_score)
        assertEquals("500", satScore?.sat_critical_reading_avg_score)

        val nonExistentScore = viewModel.getSatScoreForSchool("003")
        assertNull(nonExistentScore)
    }
}