package com.example.nycshoolapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.nycshoolapp.domain.NetworkStatus
import com.example.nycshoolapp.ui.theme.NYCShoolAppTheme
import com.example.nycshoolapp.ui.theme.screens.SchoolDetailScreen
import com.example.nycshoolapp.ui.theme.screens.SchoolListScreen
import com.example.nycshoolapp.viewmodel.SchoolViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            NYCShoolAppTheme {
                NYCSchoolsApp()
            }
        }
    }
}

@Composable
fun NYCSchoolsApp() {
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = "schoolList") {
        composable("schoolList") {
            val viewModel: SchoolViewModel = hiltViewModel()

            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Text(
                    text = "NYC SCHOOLS",
                    fontWeight = FontWeight.Bold,
                    fontSize = 25.sp,
                    modifier = Modifier
                        .padding(50.dp)
                        .align(Alignment.TopCenter)
                )
            }
            SchoolListScreen(
                viewModel = viewModel,
                onSchoolClick = { school ->
                    navController.navigate("schoolDetail/${school.dbn}")
                }
            )
        }
        composable("schoolDetail/{dbn}") { backStackEntry ->
            val viewModel: SchoolViewModel = hiltViewModel()
            val schoolsState by viewModel.schools.collectAsState()

            val dbn = backStackEntry.arguments?.getString("dbn")

            when (val status = schoolsState) {
                is NetworkStatus.Success -> {
                    val school = status.data.find { it.dbn == dbn }
                    if (school != null) {
                        SchoolDetailScreen(viewModel = viewModel, school = school)
                    } else {
                        // Handle case when school is not found
                    }
                }

                is NetworkStatus.Error -> {
                    // Handle error state
                }

                NetworkStatus.Loading -> {
                    // Show loading indicator
                }

            }
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    NYCShoolAppTheme {
        Greeting("Android")
    }
}