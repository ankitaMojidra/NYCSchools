package com.example.nycshoolapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycshoolapp.data.model.SatScore
import com.example.nycshoolapp.data.model.School
import com.example.nycshoolapp.data.repository.SchoolRepository
import com.example.nycshoolapp.domain.NetworkStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val repository: SchoolRepository) : ViewModel() {
    private val _schools = MutableStateFlow<NetworkStatus<List<School>>>(NetworkStatus.Loading)
    val schools: StateFlow<NetworkStatus<List<School>>> = _schools

    private val _satScores =
        MutableStateFlow<NetworkStatus<Map<String, SatScore>>>(NetworkStatus.Loading)
    val satScores: StateFlow<NetworkStatus<Map<String, SatScore>>> = _satScores

    init {
        loadSchoolData()
    }

    /**
     * Loads the list of schools and their SAT scores.
     */
   private fun loadSchoolData() {
        viewModelScope.launch {
            _schools.value = NetworkStatus.Loading
            _satScores.value = NetworkStatus.Loading

            val schoolsResult = repository.getSchools()
            _schools.value = schoolsResult

            val satScoresResult = repository.getSatScores()
            _satScores.value = when (satScoresResult) {
                is NetworkStatus.Success -> NetworkStatus.Success(satScoresResult.data.associateBy { it.dbn })
                is NetworkStatus.Error -> NetworkStatus.Error(satScoresResult.exception)
                NetworkStatus.Loading -> NetworkStatus.Loading
            }
        }
    }

    /**
     * Gets the SAT score for a specific school.
     *
     * @param dbn The DBN (District Borough Number) of the school.
     * @return The SatScore object for the school, or null if not found.
     */
    fun getSatScoreForSchool(dbn: String): SatScore? {
        return when (val scores = _satScores.value) {
            is NetworkStatus.Success -> scores.data[dbn]
            else -> null
        }
    }
}