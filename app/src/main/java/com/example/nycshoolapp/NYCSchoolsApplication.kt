package com.example.nycshoolapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class NYCSchoolsApplication : Application()