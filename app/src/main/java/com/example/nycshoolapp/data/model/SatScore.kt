package com.example.nycshoolapp.data.model

data class SatScore(
    val dbn: String,
    val sat_writing_avg_score: String,
    val sat_math_avg_score: String,
    val sat_critical_reading_avg_score: String,
    val school_name: String
)