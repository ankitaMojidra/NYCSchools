package com.example.nycshoolapp.data.model

data class School(
    val dbn: String,
    val school_name: String,
    val overview_paragraph: String,
    val location: String,
    val neighborhood: String,
    val phone_number: String,
    val school_email: String,
    val finalgrades: String,
    val total_students: String,
    val website: String,
    val zip: String
)
