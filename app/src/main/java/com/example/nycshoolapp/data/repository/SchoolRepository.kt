package com.example.nycshoolapp.data.repository

import com.example.nycshoolapp.data.model.School
import com.example.nycshoolapp.data.model.SatScore
import com.example.nycshoolapp.data.remote.SchoolApi
import com.example.nycshoolapp.domain.NetworkStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val api: SchoolApi) {
    /**
     * Fetches the list of NYC high schools from the API.
     *
     * @return A NetworkStatus containing a list of School objects or an error.
     */
    suspend fun getSchools(): NetworkStatus<List<School>> = withContext(Dispatchers.IO) {
        try {
            val schools = api.getSchools()
            NetworkStatus.Success(schools)
        } catch (e: Exception) {
            handleNetworkException(e)
        }
    }

    /**
     * Fetches the SAT scores for NYC high schools from the API.
     *
     * @return A NetworkStatus containing a list of SatScore objects or an error.
     */
    suspend fun getSatScores(): NetworkStatus<List<SatScore>> = withContext(Dispatchers.IO) {
        try {
            val satScores = api.getSatScores()
            NetworkStatus.Success(satScores)
        } catch (e: Exception) {
            handleNetworkException(e)
        }
    }

    private fun handleNetworkException(e: Exception): NetworkStatus.Error {
        return when (e) {
            is UnknownHostException -> NetworkStatus.Error("Unable to reach the server. Please check your internet connection.")
            is SocketTimeoutException -> NetworkStatus.Error("The connection has timed out. Please try again.")
            is IOException -> NetworkStatus.Error("An error occurred while communicating with the server. Please try again.")
            else -> NetworkStatus.Error("An unexpected error occurred: ${e.message}")
        }
    }
}