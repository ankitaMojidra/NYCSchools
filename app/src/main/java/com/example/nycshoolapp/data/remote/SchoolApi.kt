package com.example.nycshoolapp.data.remote

import com.example.nycshoolapp.data.model.SatScore
import com.example.nycshoolapp.data.model.School
import retrofit2.http.GET

interface SchoolApi {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): List<School>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatScores(): List<SatScore>
}