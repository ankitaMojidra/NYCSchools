package com.example.nycshoolapp.ui.theme.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nycshoolapp.data.model.School
import com.example.nycshoolapp.domain.NetworkStatus
import com.example.nycshoolapp.viewmodel.SchoolViewModel

@Composable
fun SchoolDetailScreen(viewModel: SchoolViewModel, school: School) {
    val satScoresStatus by viewModel.satScores.collectAsState()

    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Text(
            text = "SCHOOL DETAILS",
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp,
            modifier = Modifier
                .padding(50.dp)
                .align(Alignment.TopCenter)
        )
    }
    Column(modifier = Modifier.padding(top = 80.dp, start = 16.dp, end = 16.dp)) {
        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("School Name: ")
                }
                append(school.school_name)
            },
            modifier = Modifier.padding(top = 10.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("Total Students: ")
                }
                append(school.total_students)
            },
            modifier = Modifier.padding(top = 10.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("Website: ")
                }
                append(school.website)
            },
            modifier = Modifier.padding(top = 10.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("Phone Number: ")
                }
                append(school.phone_number)
            },
            modifier = Modifier.padding(top = 10.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("Location: ")
                }
                append(school.location)
            },
            modifier = Modifier.padding(top = 10.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(style = SpanStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp)) {
                    append("Zip: ")
                }
                append(school.zip)
            },
            modifier = Modifier.padding(top = 10.dp)
        )
        when (satScoresStatus) {
            is NetworkStatus.Loading -> Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "Loading...", fontWeight = FontWeight.Bold, fontSize = 25.sp)
            }

            is NetworkStatus.Error -> Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Unable to load SAT scores: ${(satScoresStatus as NetworkStatus.Error).exception}",
                    fontWeight = FontWeight.Bold,
                    fontSize = 25.sp
                )
            }

            is NetworkStatus.Success -> {
                val satScore = viewModel.getSatScoreForSchool(school.dbn)
                satScore?.let {
                    Box(
                        modifier = Modifier.fillMaxWidth(),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(
                            text = "SAT SCORES",
                            modifier = Modifier.padding(top = 10.dp),
                            fontWeight = FontWeight.Bold,
                            fontSize = 18.sp
                        )
                    }

                    Text(
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 16.sp
                                )
                            ) {
                                append("Reading: ")
                            }
                            append(it.sat_critical_reading_avg_score)
                        },
                        modifier = Modifier.padding(top = 10.dp)
                    )

                    Text(
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 16.sp
                                )
                            ) {
                                append("Math: ")
                            }
                            append(it.sat_math_avg_score)
                        },
                        modifier = Modifier.padding(top = 10.dp)
                    )

                    Text(
                        text = buildAnnotatedString {
                            withStyle(
                                style = SpanStyle(
                                    fontWeight = FontWeight.Bold,
                                    fontSize = 16.sp
                                )
                            ) {
                                append("Writing: ")
                            }
                            append(it.sat_writing_avg_score)
                        },
                        modifier = Modifier.padding(top = 10.dp)
                    )

                } ?: Box(
                    modifier = Modifier
                        .padding(top = 20.dp)
                        .fillMaxSize()
                ) {
                    Text(
                        text = "No SAT scores available for this school.",
                        fontWeight = FontWeight.Bold,
                        fontSize = 20.sp
                    )
                }
            }
        }
    }
}