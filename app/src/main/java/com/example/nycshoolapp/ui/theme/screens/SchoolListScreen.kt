package com.example.nycshoolapp.ui.theme.screens

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.nycshoolapp.data.model.School
import com.example.nycshoolapp.domain.NetworkStatus
import com.example.nycshoolapp.ui.theme.components.SchoolItem
import com.example.nycshoolapp.viewmodel.SchoolViewModel

@Composable
fun SchoolListScreen(viewModel: SchoolViewModel, onSchoolClick: (School) -> Unit) {
    val schoolsStatus by viewModel.schools.collectAsState()

    when (schoolsStatus) {
        is NetworkStatus.Loading -> {
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Text(text = "Loading...", fontWeight = FontWeight.Bold, fontSize = 25.sp)
            }
        }

        is NetworkStatus.Error ->
            Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                Text(
                    text = (schoolsStatus as NetworkStatus.Error).exception,
                    fontWeight = FontWeight.Bold,
                    fontSize = 25.sp
                )
            }

        is NetworkStatus.Success -> {
            val schools = (schoolsStatus as NetworkStatus.Success<List<School>>).data

            LazyColumn(modifier = Modifier.padding(top = 80.dp, bottom = 30.dp)) {
                items(schools) { school ->
                    SchoolItem(school = school, onClick = { onSchoolClick(school) })
                }
            }
        }
    }
}
